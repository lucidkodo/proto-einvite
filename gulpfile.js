const gulp = require('gulp')
const autoprefix = require('gulp-autoprefixer')
const sass = require('gulp-sass')
const pug = require('gulp-pug')
const minify = require('gulp-minify')

const { parallel, task, src, dest, watch } = gulp

task('build:css', () => {
  return src(['src/**/*.sass'])
    .pipe(sass({
      indentation: true
    }))
    .pipe(autoprefix({
      browsers: ['last 10 versions']
    }))
    .pipe(dest('public/'))
})

task('build:html', () => {
  return src(['src/**/*.pug'])
    .pipe(pug({
      pretty: true
    }))
    .pipe(dest('public/'))
})

task('build:assets', () => {
  return src(['src/**/*.{jpg,png,gif,svg,mp4}'])
    .pipe(dest('public/'))
})

task('compress', () => {
  return src(['src/*.js'])
    .pipe(minify())
    .pipe(dest('public/'))
})

task('build', parallel('build:css', 'build:html', 'build:assets', 'compress'))
task('default', parallel('build'))

task('watch:css', () => watch('src/**/*.sass', parallel('build:css')))
task('watch:html', () => watch('src/**/*.pug', parallel('build:html')))
task('watch:assets', () => watch('src/**/*.{jpg,png,gif,svg,mp4}', parallel('build:assets')))
task('watch:js', () => watch('src/*.js', parallel('compress')))

task('watch', parallel('watch:css', 'watch:html', 'watch:assets', 'watch:js'))
