require('dotenv').config()
const { promisify } = require('util')
const GS = require('google-spreadsheet')
const creds = require('./credentials.json')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')
const fs = require('fs')

const getSheet = async tab => {
  tab = tab || 0
  const wb = new GS(process.env.SHEETID)
  await promisify(wb.useServiceAccountAuth)(creds)
  const sheet = await promisify(wb.getInfo)()
  sheet.asyncRows = promisify(sheet.worksheets[tab].getRows)
  return sheet
}

const signJWT = async sheetRows => {
  for (let i = 0; i < sheetRows.length; i++) {
    const row = sheetRows[i]

    if (!row.jwt) {
      row.jwt = jwt.sign({
        email: row.email,
        exp: 1569891600
      }, process.env.NONCE)

      await row.save()
    }
  }

  console.log('Done signing jwt')
}

const blastEDM = async sheetRows => {
  for (let i = 0; i < sheetRows.length; i++) {
    const row = sheetRows[i]
    let html

    if (row.username) {
      // fs.readFile('./public/invitation.html', (err, data) => {
      fs.readFile('./src/confirmation.html', (err, data) => {
        if (err) {
          console.log(err)
        } else {
          // html = data.toString().replace('$$USERNAME$$', row.username).replace('$$JWT$$', row.jwt)

          const mailOpt = {
            from: 'Madan & Nikki <mad4nik@gmail.com>',
            to: row.email,
            // subject: 'Madan & Nikki\'s intimate reception: Let us know by 31st May 2019 😊',
            subject: 'Thank you for your RSVP',
            html: data
          }

          // // try to decode jwt (doesn't work)
          // const verified = jwt.verify(row.jwt, process.env.NONCE)
          // console.log(verified)

          // // condition for send out RSVP
          // if (!row.sent) {
          //   console.log(row.email)

          // // condition for send out CONFIRMATION
          if (row.status === 'yes' && row.sent && !row.confirmation) { // condition for send out
            console.log(row.email)

            const transporter = nodemailer.createTransport({
              service: 'gmail',
              auth: {
                user: process.env.EMAIL,
                pass: process.env.PASSWORD
              }
            })

            transporter.sendMail(mailOpt, (err, status) => {
              if (err) {
                console.log(err)
              } else {
                // row.sent = new Date() // for rsvp blasting only
                row.confirmation = new Date()
                row.save()
              }
            })
          }

        }
      })
    }
  }
  console.log('Done blasting.')
}

;(async () => {
  const action = process.argv[2]

  try {
    const data = await getSheet(1) // 0 prod, 1 uat
    const sheetRows = await data.asyncRows()

    if (action === 'sign') {
      await signJWT(sheetRows)
    } else if (action === 'blast') {
      await blastEDM(sheetRows)
    }

  } catch (e) {
    console.log(e)
  }
})()
