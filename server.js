require('dotenv').config()
const { promisify } = require('util')
const express = require('express')
// const router = require('express').Router
const GS = require('google-spreadsheet')
const creds = require('./credentials.json')
const jwt = require('jsonwebtoken')
// const nodemailer = require('nodemailer')
// const fs = require('fs')

const app = express()

app.use(express.json({}))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use(express.urlencoded({
  extended: true
}))

const getSheet = async tab => {
  tab = tab || 0
  const wb = new GS(process.env.SHEETID)
  await promisify(wb.useServiceAccountAuth)(creds)
  const sheet = await promisify(wb.getInfo)()
  sheet.asyncRows = promisify(sheet.worksheets[tab].getRows)
  return sheet
}


app.get('/api/verify/:codeStr', async (req, res) => {
  try {
    const codeStr = req.params.codeStr
    console.log('verifying ' + codeStr)
    const data = await promisify(jwt.verify)(codeStr, process.env.NONCE)
    const sheet = await getSheet()
    const db = await sheet.asyncRows()
    const mainuser = db.find(row => row.email === data.email)

    if (mainuser) {
      console.log('verified as ' + mainuser.email)
      const rows = db.filter(row => row.email === data.email)

      const result = {
        name: mainuser.username,
        status: mainuser.status,
        submitted: mainuser.submitted,
        email: mainuser.email,
        pax: []
      }

      for (let i =  0; i < rows.length; i++) {
        result.pax.push({
          name: rows[i].guestname || '',
          meat: rows[i].meat || 'chicken'
        })
      }

      return res.json(result)
    } else {
      // return user not found
      console.log('user ' + data.email + ' not found.')
    }
  } catch (err) {
    console.log(err)
  }
})

app.post('/api/submit/', async (req, res) => {
  try {
    const rsvp = req.body
    await jwt.verify(rsvp.jwt, process.env.NONCE)

    const sheet = await getSheet()
    const db = await sheet.asyncRows()
    const matched = db.filter(record => record.email === rsvp.email)

    if (matched.length) {
      for (let i = 0; i < matched.length; i++) {
        const row = matched[i]

        if (row.submitted) {
          // res.send({redirect: '/faq.html'})
          break
        } else {
          if (matched[i].username !== '') {
            matched[i].submitted = new Date()
            matched[i].status = rsvp.status
          }

          if (rsvp.guests[i]) {
            matched[i].guestname = rsvp.guests[i].name
            matched[i].meat = rsvp.guests[i].meat
          }

          await matched[i].save()
        }
      }
    }

    res.send({redirect: '/faq.html'})
  } catch (err) {
    console.log(err)
    // redirect "something went wrong, please contact nikki"
  }
})

app.listen(9999, () => {
  console.log('app started on 9999.')
})
