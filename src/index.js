window.Vue = new Vue({
  el: '#app',
  data: {
    message: 'hello',
    verified: false
  },

  async created () {
    // console.log(window.location.search)
    this.verified = await window.fetch('http://localhost:9999/verify/' + window.location.search.split('?')[1], {
      mode: 'no-cors'
    })
    console.log(this.verified)
  }
})
